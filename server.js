const express = require('express');
const bodyParser = require('body-parser');
const db = require('./db');
//const router = require('./components/message/network');
const router = require('./network/routes');
const uri = 'mongodb+srv://user_db:user123@cluster0-hzovq.mongodb.net/telegrom?ssl=true&replicaSet=Main-shard-0&authSource=admin&retryWrites=true';
db(uri);
var app = express();

app.use(bodyParser.json());
//app.use(router);

router(app);

app.use('/app', express.static('public'));
app.listen(3000);
console.log("The aplication listening in port 3000");