const db = require('mongoose');
db.Promise = global.Promise;

async function connect(uri) {
    //const uri = 'mongodb+srv://user_db:user123@cluster0-hzovq.mongodb.net/telegrom?ssl=true&replicaSet=Main-shard-0&authSource=admin&retryWrites=true'
    await db.connect(uri, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }).then(() => console.log('[db] conectada con exito')).catch(e => console.log('db error!!!!!!', e));

}
module.exports = connect;

//a2fehzhXIrdsThM5
//mongodb+srv://db_user:<password>@cluster0-pfkhh.mongodb.net/test?retryWrites=true&w=majority
//mongodb+srv://db_user:<password>@cluster0-pfkhh.mongodb.net/telegrom
//const uri = 'mongodb://user_db:<password>@cluster0-shard-00-00-hzovq.mongodb.net:27017,cluster0-shard-00-01-hzovq.mongodb.net:27017,cluster0-shard-00-02-hzovq.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority';
//const uri = 'mongodb+srv://user_db:a2fehzhXIrdsThM5@@cluster0-hzovq.mongodb.net/';